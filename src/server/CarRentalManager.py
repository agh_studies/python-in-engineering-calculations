import os
import json

class CarRentalManager():

    def __init__(self, configurations_path, debug):
        #------------- general
        self.debug = debug
        #------------- paths
        self.configurations_path = configurations_path
        self.car_data_path = self.configurations_path + "/car_data.json"
        self.user_data_path = self.configurations_path + "/user_data.json"
        #------------- config files
        self.cars_file = None
        self.users_file = None
        #------------- load config files
        with open(os.path.abspath(self.car_data_path)) as json_file:
            self.cars_file = json.load(json_file)
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)

    def is_user_logged(self, username):
        # =======================================================================================
        # This method check that user is currently logged
        # username - username (string)
        # Returns True if user is logged
        # =======================================================================================
        result = False
        found_user = False
        #update users data
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current user exists in local database
        print("[CarRentalManager] Checking user status...");
        for user in self.users_file:
            if(self.users_file[user]['username'] == username):
                found_user = True
                if(self.users_file[user]['status'] == "logged"):
                    result = True
                    print("[CarRentalManager] User is logged");
                break
        #if no found user 
        if(found_user == False):
            print("User: " + username + " not found. User exists?")
            
        return result

    def save_json(self, json_file, json_path):
        # =======================================================================================
        # This method is save json data to json_file
        # json_file - json data to be saved
        # json_path - path to json file
        # =======================================================================================
        with open(json_path, 'w') as outfile:
            json.dump(json_file, outfile)
        # if(self.debug):
        #     print("Save json to: " + json_path)

    def login_user(self, username, password):
        # =======================================================================================
        # This method is responsinble for validate user login process.
        # username - user login value (string)
        # password - user password value (string)
        # Returns True (if login is succeed) or False
        # =======================================================================================
        print("[CarRentalManager] Executing login_user request");
        result = False
        #update users data
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current user exists in local database
        for user in self.users_file:
            if(self.users_file[user]['username'] == username and self.users_file[user]['password'] == password):
                print("[CarRentalManager] Found user with username: " + username + ", name: " + self.users_file[user]['name'] + ", surname: " + self.users_file[user]['surname'])
                #change status flag to logged
                self.users_file[user]['status'] = "logged"
                print("[CarRentalManager] User " + username + " has been logged")
                result = True
                #save json
                self.save_json(self.users_file, self.user_data_path)
                break
        if(result == False):
            #if no found user 
            print("[CarRentalManager] " + "User: " + username + " not found. Maybe password is wrong?")
            result = False

        return result

    def logout_user(self, username):
        # =======================================================================================
        # This method is responsinble for logut process.
        # username - user login value (string)
        # Returns True (if login is succeed) or False
        # =======================================================================================
        print("[CarRentalManager] Executing logout_user request");
        result = False
        found_user = False
        #update users data
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current user exists in local database
        for user in self.users_file:
            if(self.users_file[user]['username'] == username):
                found_user = True
                self.users_file[user]['status'] = "logout"
                print("[CarRentalManager] User " + username + " is logout")
                result = True
                #save json
                self.save_json(self.users_file, self.user_data_path)
                break
        #if no found user 
        if(found_user == False):
            print("User: " + username + " not found. User exists?")

        return result

    def register_user(self, name, surname, username, password):
        # =======================================================================================
        # Add new user. Firstly check if user with this username or password does not exist.
        # name - user name (string)
        # surname - user surname (string)
        # username - user login value (string)
        # password - user password value (string)
        # Returns True (if register is succeed) or False
        # =======================================================================================
        print("[CarRentalManager] Executing register_user request");
        result = False
        user_exists = False
        #update users data
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current user exists in local database
        for user in self.users_file:
            if(self.users_file[user]['username'] == username or self.users_file[user]['password'] == password):
                print("[CarRentalManager] This user currently exists, username: " + username + ", password: " + password + ". Register process failed")
                user_exists = True
                result = False
                break
        #register new user
        if(user_exists == False):
            user_id = len(self.users_file) + 1
            try:
                #add new fields to json file
                self.users_file[str(user_id)] = {}
                self.users_file[str(user_id)]['id'] = user_id
                self.users_file[str(user_id)]['name'] = name
                self.users_file[str(user_id)]['surname'] = surname
                self.users_file[str(user_id)]['username'] = username
                self.users_file[str(user_id)]['password'] = password
                self.users_file[str(user_id)]['status'] = "logout"
                self.users_file[str(user_id)]['rented_car'] = {}
                print("[CarRentalManager] New user has been registered with username: " + username + " and password: " + password) 
                result = True
                #save json
                self.save_json(self.users_file, self.user_data_path)
            except:
                print("[CarRentalManager] Incorrect data to register user!") 
                result = False
        
        return result

    def delete_user(self, username, password):
        # =======================================================================================
        # Delete user. Firstly check if user with this username and password exists.
        # username - user login value (string)
        # password - user password value (string)
        # Returns True (if delete is succeed) or False
        # =======================================================================================
        print("[CarRentalManager] Executing delete_user request");
        result = False
        #update users data
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current user exists in local database
        for user in self.users_file:
            if(self.users_file[user]['username'] == username or self.users_file[user]['password'] == password):
                print("[CarRentalManager] Delete user, username: " + username + ", password: " + password)
                del self.users_file[user]
                result = True
                #save json
                self.save_json(self.users_file, self.user_data_path)
                break

        if(result == False):
            #if no found user 
            print("[CarRentalManager] No user found to delete, username: " + username + ", password: " + password)

        return True

    def get_available_cars(self):
        # =======================================================================================
        # Find all available cars in car_data.json local database.
        # preferred_type - preffered car type (string)
        # Returns dictionary with car results
        # =======================================================================================
        print("[CarRentalManager] Executing get_available_cars request");
        results = {}
        #update car data
        with open(os.path.abspath(self.car_data_path)) as json_file:
            self.cars_file = json.load(json_file)
        #check if current car exists in local database
        for car in self.cars_file:
            if(self.cars_file[car]['status'] == "free"):
                #add car to results
                result_id = len(results) + 1
                results[str(result_id)] = {}
                results[str(result_id)]['id'] = self.cars_file[car]['id']
                results[str(result_id)]['type'] = self.cars_file[car]['type']
                results[str(result_id)]['model'] = self.cars_file[car]['model']
                results[str(result_id)]['status'] = self.cars_file[car]['status']
        
        return results

    def get_rented_cars(self, username):
        # =======================================================================================
        # Find all rented for logged user
        # username - user login value (string)
        # Returns dictionary with car results
        # =======================================================================================
        print("[CarRentalManager] Executing get_rented_cars request");
        results = {}
        #update car data
        with open(os.path.abspath(self.car_data_path)) as json_file:
            self.cars_file = json.load(json_file)
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current car exists in local database
        for user in self.users_file:
            if(self.users_file[user]['username'] == username):
                #get rented car lists
                results = self.users_file[user]['rented_car']

        return results

    def rent_car(self, username, car_id):
        # =======================================================================================
        # Rent a car with car_id
        # car_id - car unique (int)
        # Returns True (if renting is succeed) or False
        # =======================================================================================
        print("[CarRentalManager] Executing rent_car request");
        found_car = False;
        result = False
        #update car data
        with open(os.path.abspath(self.car_data_path)) as json_file:
            self.cars_file = json.load(json_file)
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current car exists in local database
        for car in self.cars_file:
            if(self.cars_file[car]['id'] == car_id):
                found_car = True;
                #check if car is available
                if(self.cars_file[car]['status'] == "free"):
                    self.cars_file[car]['status'] = "rented"
                    print("[CarRentalManager] Car " + self.cars_file[car]['type'] + self.cars_file[car]['model'] + " has been rented")
                    #update user file
                    for user in self.users_file:
                        if(self.users_file[user]['username'] == username):
                            index = len(self.users_file[user]['rented_car']) + 1
                            self.users_file[user]['rented_car'][str(index)] = {}
                            self.users_file[user]['rented_car'][str(index)]['id'] = self.cars_file[car]['id']
                            self.users_file[user]['rented_car'][str(index)]['type'] = self.cars_file[car]['type']
                            self.users_file[user]['rented_car'][str(index)]['model'] = self.cars_file[car]['model']
                            print("[CarRentalManager] User" + username + " user_data updated with new car")
                            result = True

                    #save json
                    self.save_json(self.users_file, self.user_data_path)
                    self.save_json(self.cars_file, self.car_data_path)
                else:
                    print("[CarRentalManager] Car " + self.cars_file[car]['type'] + self.cars_file[car]['model'] + " is not available for renting.")
                    result = False
                break
        if(found_car == False):
            print("[CarRentalManager] Car " + self.cars_file[car]['type'] + self.cars_file[car]['model'] + " is not exist.")
            result = False

        return result

    def return_car(self, username, car_id):
        # =======================================================================================
        # Return rented car with car_id
        # username - user login value (string)
        # password - user password value (string)
        # car_id - car unique (int)
        # Returns True (if returning is succeed) or False
        # =======================================================================================
        print("[CarRentalManager] Executing return_car request");
        result = False
        #update car data
        with open(os.path.abspath(self.car_data_path)) as json_file:
            self.cars_file = json.load(json_file)
        with open(os.path.abspath(self.user_data_path)) as json_file:
            self.users_file = json.load(json_file)
        #check if current car exists in local database
        for car in self.cars_file:
            if(self.cars_file[car]['id'] == car_id):
                #check if car is available
                if(self.cars_file[car]['status'] == "rented"):
                    self.cars_file[car]['status'] = "free"   
                    print("[CarRentalManager] Car " + self.cars_file[car]['type'] + self.cars_file[car]['model'] + " has been returned from cars database")
                    #delete car in user_data
                    for user in self.users_file:
                        if(self.users_file[user]['username'] == username):
                            #get rented car lists
                            user_car_list = self.users_file[user]['rented_car']
                            for user_car in user_car_list:
                                if(user_car_list[user_car]['id'] == car_id): 
                                    print("[CarRentalManager] Car " + user_car_list[user_car]['type'] + user_car_list[user_car]['model'] + " has been returned from users database")
                                    del user_car_list[user_car]
                                    result = True
                                    break;
                    #save json
                    self.save_json(self.users_file, self.user_data_path)
                    self.save_json(self.cars_file, self.car_data_path)
                else:
                    print("[CarRentalManager] Car " + self.cars_file[car]['type'] + self.cars_file[car]['model'] + " can not be returned.")
                    result = False
                break

        return result
