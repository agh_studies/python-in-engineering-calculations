import os
import json
from ServerManager import ServerManager
import argparse

configurations_path = "/Users/macbook/Documents/Studia/mgr/semestr_2/Python/python-in-engineering-calculations/src/configurations"
debug = False

parser = argparse.ArgumentParser(description='Input debug state')
parser.add_argument('--debug', default=False, help="Insert debug output for script")
args = parser.parse_args()
debug = args.debug

if __name__ == '__main__':
    print("[MAIN] Server script started")
    print("[INFO] debug: " + str(debug))
    server_manager = ServerManager(configurations_path, debug)
    server_manager.start_listening()
    #program loop
    while True:
        server_manager.server_loop()