import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import os
import sys
import json
from CarRentalManager import CarRentalManager

class ServerManager():

    def __init__(self, configurations_path, debug):
        #------------- system
        self.debug = debug
        #------------- paths
        self.configurations_path = configurations_path
        self.config_path = self.configurations_path + "/config.json"
        #------------- mqtt data
        self.server_address = None
        self.server_port = None
        self.mqtt_client = mqtt.Client()
        self.topics = []
        self.topics.append("/car_rental/server/#")
        #------------- config files
        self.config_file = None
        #------------- load config files
        with open(os.path.abspath(self.config_path)) as json_file:
            self.config_file = json.load(json_file)
            self.server_address = self.config_file['server_address']
            self.server_port = self.config_file['server_port']
        #------------- create CarRentalManager object
        self.car_rental_manager = CarRentalManager(self.configurations_path, debug)

    def start_listening(self):
        # =======================================================================================
        # Function which start connection with MQTT Broker. Create bindings with callbacks
        # server_address - MQTT Broker address
        # server_port - MQTT Broker port
        # =======================================================================================
        #------------- Create bindings
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        #------------- Create connection
        if( self.server_address != None and self.server_port != None):
            print("[ServerManager] Starting connection to the server: " + str(self.server_address) + ", port: " + str(self.server_port))
            self.mqtt_client.connect(self.server_address, self.server_port, 60)
            print("[ServerManager] Start server loop"); 

    def server_loop(self):
        # =======================================================================================
        # Function starts a new thread, that calls the loop method at regular intervals
        # =======================================================================================
        self.mqtt_client.loop_start()

    def on_connect(self, client, userdata, flags, rc):
        # =======================================================================================
        # The callback for when the client receives a CONNACK response from the server
        # client - mqtt client object address
        # userdata - mqtt client data
        # flags - additional user flags
        # rc - connection status
        # =======================================================================================
        if(rc==0):
            print("[ServerManager] Connection successully created")
            #start topic subscription
            for topic in self.topics:
                self.mqtt_client.subscribe(topic)
                print("[ServerManager] Start subscribing topic: " + topic)
        else:
            print("[ServerManager] Someting went wrong during making connecton. Try again")

    def on_message(self, client, userdata, msg):
        # =======================================================================================
        # The callback for when a PUBLISH message is received from the server.
        # client - mqtt client object address
        # userdata - mqtt client data
        # msg - message payload object
        # =======================================================================================
        print("------------------------------------------------------------------------------------------------")
        if(self.debug):
            print("[ServerManager] Receive MQTT message, topic: " + msg.topic + ", payload: " + str(msg.payload))
        self.handle_message(msg)
        print("------------------------------------------------------------------------------------------------")

    def handle_message(self, msg):
        # =======================================================================================
        # Function responsible for handling and proccessing incomming MQTT messages from clients.
        # msg - incomming message in json format
        # =======================================================================================
        #convert bytes to string
        payload_string = (msg.payload).decode("utf-8") 
        #convert string to json format
        payload_json = json.loads(payload_string)
        message_head = "/car_rental/server/"
        response_head = "/car_rental/client/" + str(payload_json['username']) + "/"

        print("[ServerManager] Start handling message for user: " + payload_json['username']);

        try:
            #-------------------------------------------------------------------------- register user
            if msg.topic == message_head + "register_user":
                result = self.car_rental_manager.register_user(payload_json['name'], payload_json['surname'], payload_json['username'], payload_json['password'])
                self.publish_message("/car_rental/client/register_user", result)
             #-------------------------------------------------------------------------- login user
            elif msg.topic == message_head + "login_user":
                result = self.car_rental_manager.login_user(payload_json['username'], payload_json['password'])
                self.publish_message(response_head + "login_user", result)
            #-------------------------------------------------------------------------- delete user
            elif msg.topic == message_head + "delete_user":
                if(self.car_rental_manager.is_user_logged(payload_json['username'])):
                    result = self.car_rental_manager.delete_user(payload_json['username'], payload_json['password'])
                    self.publish_message(response_head+"delete_user", result)
                else:
                    self.publish_message(response_head + "delete_user", "User is log out. To perform an action please try to log in")
            #-------------------------------------------------------------------------- logout user
            elif msg.topic == message_head + "logout_user":
                if(self.car_rental_manager.is_user_logged(payload_json['username'])):
                    result = self.car_rental_manager.logout_user(payload_json['username'])
                    self.publish_message(response_head+"logout_user", result)
                else:
                    self.publish_message(response_head + "logout_user", "User is log out. To perform an action please try to log in")
            #-------------------------------------------------------------------------- get available cars
            elif msg.topic == message_head + "get_available_cars":
                if(self.car_rental_manager.is_user_logged(payload_json['username'])):
                    result = self.car_rental_manager.get_available_cars()
                    self.publish_message(response_head+"get_available_cars", result)
                else:
                    self.publish_message(response_head + "get_available_cars", "User is log out. To perform an action please try to log in")
            #-------------------------------------------------------------------------- get rented cars
            elif msg.topic == message_head + "get_rented_cars":
                if(self.car_rental_manager.is_user_logged(payload_json['username'])):
                    result = self.car_rental_manager.get_rented_cars(payload_json['username'])
                    self.publish_message(response_head+"get_rented_cars", result)
                else:
                    self.publish_message(response_head + "get_rented_cars", "User is log out. To perform an action please try to log in")
            #-------------------------------------------------------------------------- rent car
            elif msg.topic == message_head + "rent_car":
                if(self.car_rental_manager.is_user_logged(payload_json['username'])):
                    result = self.car_rental_manager.rent_car(payload_json['username'], payload_json['car_id'])
                    self.publish_message(response_head+"rent_car", result)
                else:
                    self.publish_message(response_head + "rent_car", "User is log out. To perform an action please try to log in")
            #-------------------------------------------------------------------------- return car
            elif msg.topic == message_head + "return_car":
                if(self.car_rental_manager.is_user_logged(payload_json['username'])):
                    result = self.car_rental_manager.return_car(payload_json['username'], payload_json['car_id'])
                    self.publish_message(response_head+"return_car", result)
                else:
                    self.publish_message(response_head + "return_car", "User is log out. To perform an action please try to log in")
            #-------------------------------------------------------------------------- incorrect request
            else:
                print("[ServerManager] Receive incorrect request!")
        except:
            e = sys.exc_info()[0]
            print("[ServerManager] Error during " + str(topic) + ": " + str(e))

    def publish_message(self, topic, payload):
        # =======================================================================================
        # Send single MQTT message to custom topic (client) with payload
        # topic - topic on which the message is to be sent
        # payload - message payload
        # =======================================================================================
        print("[ServerManager] Response sending to the client...")
        if(self.debug):
            print("[ServerManager] Publish MQTT message, topic:  " + topic + ", payload: " + str(payload))
        msg_payload = json.dumps(payload).encode('utf-8')
        publish.single(topic=topic, payload=msg_payload, hostname=self.server_address, port=self.server_port)
        print("[ServerManager] Response sent successfully")
