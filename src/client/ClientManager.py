import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import os
import sys
import json
import threading
from time import sleep

class ClientManager():

    def __init__(self, configurations_path, debug):
        #------------- system
        self.debug = debug
        self.program_loop = True
        #------------- paths
        self.configurations_path = configurations_path
        self.config_path = self.configurations_path + "/config.json"
        #------------- mqtt data
        self.server_address = None
        self.server_port = None
        self.mqtt_client = mqtt.Client()
        self.topics = []
        self.topics.append("/car_rental/client/#")
        #------------- config files
        self.config_file = None
        #------------- load config files
        with open(os.path.abspath(self.config_path)) as json_file:
            self.config_file = json.load(json_file)
            self.server_address = self.config_file['server_address']
            self.server_port = self.config_file['server_port']
        #------------- handling mqtt responses
        self.waiting_for_response = False
        #------------- user data
        self.is_logged = False
        self.username = ""
        self.password = ""

    # ---------------------------------------------------------- MQTT ----------------------------------------------------------

    def start_listening(self):
        # =======================================================================================
        # Function which start connection with MQTT Broker. Create bindings with callbacks
        # server_address - MQTT Broker address
        # server_port - MQTT Broker port
        # =======================================================================================
        #------------- Create bindings
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        #------------- Create connection
        if( self.server_address != None and self.server_port != None):
            if(self.debug):
                print("[ClientManager] Starting connection to the server: " + str(self.server_address) + ", port: " + str(self.server_port))
            self.mqtt_client.connect(self.server_address, self.server_port, 60)
            if(self.debug):
                print("[ClientManager] Start server loop"); 
    
    def client_loop(self):
        # =======================================================================================
        # Function starts a new thread, that calls the loop method at regular intervals
        # =======================================================================================
        self.mqtt_client.loop_start()

    def on_connect(self, client, userdata, flags, rc):
        # =======================================================================================
        # The callback for when the client receives a CONNACK response from the server
        # client - mqtt client object address
        # userdata - mqtt client data
        # flags - additional user flags
        # rc - connection status
        # =======================================================================================
        if(rc==0):
            if(self.debug):
                print("[ClientManager] Connection successully created")
            #start topic subscription
            for topic in self.topics:
                self.mqtt_client.subscribe(topic)
                if(self.debug):
                    print("[ClientManager] Start subscribing topic: " + topic)
            #start interface thread
            self.run_interface_thread()
        else:
            if(self.debug):
                print("[ClientManager] Someting went wrong during making connecton. Try again")

    def on_message(self, client, userdata, msg):
        # =======================================================================================
        # The callback for when a PUBLISH message is received from the server.
        # client - mqtt client object address
        # userdata - mqtt client data
        # msg - message payload object
        # =======================================================================================
        if(self.debug):
            pass
            #print("[ClientManager] Receive MQTT message, topic: " + msg.topic + ", payload: " + str(msg.payload))
        if(self.waiting_for_response == True):
            print("------------------------------------------------------------------------------------------------")
            self.message_decoder(msg)
            #set waiting_for_response to False to avoid unwanted messages
            self.waiting_for_response = False
            print("------------------------------------------------------------------------------------------------")

    def message_decoder(self, msg):
        # =======================================================================================
        # Function responsible for handling and proccessing incomming MQTT responses from server if waiting_for_response flag is set.
        # msg - incomming message in json format
        # =======================================================================================
        #convert bytes to string
        payload_string = (msg.payload).decode("utf-8") 
        #convert string to json format
        payload_json = json.loads(payload_string)

        #print("[Reponse] " + str(payload_json))
        
        #------------------------------ register process
        if(msg.topic == "/car_rental/client/register_user"):
            if(payload_string == "true"):
                #register success
                print("[Server Reponse] New user has been registered")
            else:
                #register failed
                print("[Server Reponse] Cannout register new user")
        #------------------------------ login process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/login_user"):
            if(payload_string== "true"):
                #login success
                print("[Server Reponse] " + "user: " + str(self.username) + " login success")
                self.is_logged = True
            else:
                #login failed
                print("[Server Reponse] " + "user: " + str(self.username) + " login failed")
                self.is_logged = False
        #------------------------------ delete user process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/delete_user"):
            if(payload_string == "true"):
                #deletion success
                print("[Server Reponse] " + "user: " + str(self.username) + " has been deleted")
                self.is_logged = False
            else:
                #deletion failed
                print("[Server Reponse] " + "user: " + str(self.username) + " cannot be deleted")
                self.is_logged = True
        #------------------------------ logout process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/logout_user"):
            if(payload_string == "true"):
                #logout success
                print("[Server Reponse] " + "user: " + str(self.username) + " has been logout")
                self.is_logged = False
            else:
                #login failed
                print("[Server Reponse] " + "user: " + str(self.username) + " cannot be logout")
                self.is_logged = True      
        #------------------------------ get available cars process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/get_available_cars"):
            available_cars = payload_json
            for car in available_cars:
                print("------- Car: " + str(car) + " -------")
                print("id: " + str(available_cars[car]['id']))
                print("type: " + str(available_cars[car]['type']))
                print("model: " + str(available_cars[car]['model']))
                print("status: " + str(available_cars[car]['status']))
                print("----------------------")
        #------------------------------ get rented cars process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/get_rented_cars"):
            rented_cars = payload_json
            for car in rented_cars:
                print("------- Car: " + str(car) + " -------")
                print("id: " + str(rented_cars[car]['id']))
                print("type: " + str(rented_cars[car]['type']))
                print("model: " + str(rented_cars[car]['model']))
                print("----------------------")
        #------------------------------ rent car process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/rent_car"):
            if(payload_string == "true"):
                #rent success
                print("[Server Reponse] " + "user: " + str(self.username) + " rented the car")
            else:
                #rent failed
                print("[Server Reponse] " + "user: " + str(self.username) + " cannot rent the car")
        #------------------------------ return car process
        elif(msg.topic == "/car_rental/client/" + str(self.username) +"/return_car"):
            if(payload_string == "true"):
                #return success
                print("[Server Reponse] " + "user: " + str(self.username) + " car has been returned")
            else:
                #return failed
                print("[Server Reponse] " + "user: " + str(self.username) + " cannot return the car")
                
    # ---------------------------------------------------------- MENU ----------------------------------------------------------

    def run_interface_thread(self):
        # =======================================================================================
        # Function which starts user interface as a separate thread
        # =======================================================================================
        interface_thread = threading.Thread(target=self.client_interface)
        interface_thread.start()

    def client_interface(self):
        # =======================================================================================
        # Console user interaface
        # =======================================================================================
        while(self.program_loop):
            print("\n--------------CLIENT MENU--------------")
            print("| 1. Login                            |")
            print("| 2. Register                         |")
            print("| 3. Exit                             |")
            print("---------------------------------------")
            result = self.input_number("[ClientManager] Choose option", 1, 3)
            if(result == 1):
                self.login_user_menu()
            elif(result == 2):
                self.register_user_menu()
            elif(result == 3):
                print("[ClientManager] Ending program...")
                self.program_loop = False

    def login_user_menu(self):
        # =======================================================================================
        # Context login menu
        # =======================================================================================
        print("\n--------------LOGIN MENU---------------")
        self.username = self.input_string("Please insert username")
        self.password = self.input_string("Please insert password")
        self.login_user(self.username, self.password)
        #check result
        if(self.is_logged==True):
            self.car_rental_menu()
        else:
            #back to CLIENT MENU
            pass
        
    def register_user_menu(self):
        # =======================================================================================
        # Context register menu
        # =======================================================================================
        print("\n--------------NEW USER REGISTRATION MENU---------------")
        temp_name = self.input_string("Please insert name")
        temp_surname = self.input_string("Please insert surname")
        temp_username = self.input_string("Please insert username")
        temp_password = self.input_string("Please insert password")
        self.register_user(temp_name, temp_surname, temp_username, temp_password)

    def car_rental_menu(self):
        # =======================================================================================
        # Context car rental menu
        # =======================================================================================
        while(self.is_logged == True):
            print("\n--------------CAR RENTAL MENU----------------")
            print("| 1. Get available cars                      |")
            print("| 2. Get rented cars                         |")
            print("| 3. Rent the car                            |")
            print("| 4. Return the car                          |")
            print("| 5. Delete account                          |")
            print("| 6. Logout                                  |")
            print("----------------------------------------------")
            result = self.input_number("["+self.username+"] " + "Choose option", 1, 5)
            if(result == 1):
                self.get_available_cars()
            elif(result == 2):
                self.get_rented_cars()
            elif(result == 3):
                self.rent_the_car()
            elif(result == 4):
                self.return_the_car()
            elif(result == 5):
                self.delete_account()
            elif(result == 6):
                self.logout() 

    # -------------------------------------------------------- FUNCTIONS --------------------------------------------------------

    def login_user(self, username, password):
        # =======================================================================================
        # Funtion responsible for process user login
        # =======================================================================================
        print("Trying to login: " + username)
        #create message to be send
        topic = "/car_rental/server/login_user"
        msg_payload = {}
        msg_payload['username'] = str(username)
        msg_payload['password'] = str(password)
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def register_user(self, name, surname, username, password):
        # =======================================================================================
        # Funtion responsible for process register user
        # =======================================================================================
        print("Trying to register new user: " + username)
        #create message to be send
        topic = "/car_rental/server/register_user"
        msg_payload = {}
        msg_payload['name'] = str(name)
        msg_payload['surname'] = str(surname)
        msg_payload['username'] = str(username)
        msg_payload['password'] = str(password)
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def get_available_cars(self):
        # =======================================================================================
        # Funtion responsible for get available cars from server
        # =======================================================================================
        #create message to be send
        topic = "/car_rental/server/get_available_cars"
        msg_payload = {}
        msg_payload['username'] = str(self.username)
        msg_payload['password'] = str(self.password)
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def get_rented_cars(self):
        # =======================================================================================
        # Funtion responsible for get available rented from server
        # =======================================================================================
        #create message to be send
        topic = "/car_rental/server/get_rented_cars"
        msg_payload = {}
        msg_payload['username'] = str(self.username)
        msg_payload['password'] = str(self.password)
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def rent_the_car(self):
        # =======================================================================================
        # Funtion responsible for execute renting car process
        # =======================================================================================
        #create message to be send
        topic = "/car_rental/server/rent_car"
        msg_payload = {}
        msg_payload['username'] = str(self.username)
        msg_payload['password'] = str(self.password)
        #get car_id
        car_id = self.input_value("["+self.username+"] " + "Input car id to rent")
        msg_payload['car_id'] = car_id
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def return_the_car(self):
        # =======================================================================================
        # Funtion responsible for execute return car process
        # =======================================================================================
        #create message to be send
        topic = "/car_rental/server/return_car"
        msg_payload = {}
        msg_payload['username'] = str(self.username)
        msg_payload['password'] = str(self.password)
        #get car_id
        car_id = self.input_value("["+self.username+"] " + "Input car id to return")
        msg_payload['car_id'] = car_id
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def delete_account(self):
        # =======================================================================================
        # Funtion responsible for delete user account process
        # =======================================================================================
        #create message to be send
        topic = "/car_rental/server/delete_user"
        msg_payload = {}
        msg_payload['username'] = str(self.username)
        msg_payload['password'] = str(self.password)
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    def logout(self):
        # =======================================================================================
        # Funtion responsible for execute user logout
        # =======================================================================================
        #create message to be send
        topic = "/car_rental/server/logout_user"
        msg_payload = {}
        msg_payload['username'] = str(self.username)
        msg_payload['password'] = str(self.password)
        #send message
        self.publish_message(topic, msg_payload)
        #wait for server response
        while(self.waiting_for_response):
            pass

    # ---------------------------------------------------------- TOOLS ----------------------------------------------------------

    def input_number(self, title, min_value, max_value):
        result = 0
        try:
            result = int(input(title + ": "))
        except:
            print("Incorrent input. Try again")
        
        return result

    def input_value(self, title):
        result = 0
        try:
            result = int(input(title + ": "))
        except:
            print("Incorrent input. Try again")
        
        return result

    def input_string(self, title):
        result = ""
        try:
            result = str(input(title + ": "))
        except:
            print("Incorrent input. Try again")
        
        return result

    def print_client(self, data):
        print("[" + self.username + "] " + data)

    def publish_message(self, topic, payload):
        # =======================================================================================
        # Send single MQTT message to custom topic (client) with payload
        # topic - topic on which the message is to be sent
        # payload - message payload
        # =======================================================================================
        self.print_client("Send message to the server...")
        if(self.debug):
            self.print_client("Publish MQTT message, topic:  " + topic + ", payload: " + str(payload))
        msg_payload = json.dumps(payload).encode('utf-8')
        publish.single(topic=topic, payload=msg_payload, hostname=self.server_address, port=self.server_port)
        self.print_client("Message sent successfully")
        #change waiting_for_response flag for capture incomming server message
        self.waiting_for_response = True

    # ---------------------------------------------------------------------------------------------------------------------------