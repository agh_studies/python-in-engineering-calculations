import os
import json
from ClientManager import ClientManager
import argparse

configurations_path = "/Users/macbook/Documents/Studia/mgr/semestr_2/Python/python-in-engineering-calculations/src/configurations"
debug = False

parser = argparse.ArgumentParser(description='Input debug state')
parser.add_argument('--debug', default=False, help="Insert debug output for script")
args = parser.parse_args()
debug = args.debug

if __name__ == '__main__':
    print("[MAIN] Client script started")
    print("[INFO] debug: " + str(debug))
    client_manager = ClientManager(configurations_path, debug)
    client_manager.start_listening()
    #program loop
    while client_manager.program_loop:
        client_manager.client_loop()

        