<div align="center">

# Car Rental Simulator - Python in Engineering Calculations
<img src="img/agh_logo.png">
</div>

<div align="center">

<br><b>Author:</b> Paweł Gontarz</br>
<br><b>Country:</b> Poland</br>
<br><b>Faculty:</b> Mechanical Engineering and Robotics Department</br>
<br><b>Field of study:</b> Mechatronics Engineering</br>

</div>

---
## Project description

<div align="justify">


The main purpose of this project was to create a car rental service simulator. Projects consists of 2 mainly parts: server side and clients side. Server is responsible for handling and processing requests. Client is able to send different request to the server like get information about available cars or rent some specific car. Server and client scripts communicates via <b>MQTT</b> protocol. Programming languge is <b>Python</b>. 
</div>


- **Server functions:**
    - Provides possibility to register new user
    - Provides possibility to login registered user
    - Sends information about all available cars to rent
    - Sends information about rented cars by logged user
    - Regularly updates local user database and car databse
    - Read/write data to file (json format)
</br>

- **Client functions:**
    - Sends request for registering new user
    - Sends request for login existing user
    - Sends request for available cars
    - Rent new car
    - Return rented car
    - Sends request for logout

---
## About MQTT
<div align="justify">
<b>MQTT</b> (<b>M</b>essage <b>Q</b>ueing <b>T</b>elemetry <b>T</b>ransport) is a lightweight communication protocol, which based on publish/subscribe system. Data exchanging process is bidirectional what means, that we can communicate in both directions. This solution is very popular in IoT (Internet of Things) and M2M (Machine to machine) systems.

<b></b>
MQTT allows to send messages asynchronously between publishers and subscribers. What is important, MQTT client can be a subscriber and publisher in the same time. Message consists of <b>topic</b> and <b>payload</b>. 

##### Topic
In MQTT, topic is a string that the broker uses to filter messages for each connected client. The topic consists of one or more topic levels. Each topic level is separated by a forward slash, for example:
```console
myhome/groundfloor/livingroom/temperature
```
We can also create multi-level topic subscription using #, for example:

```console
myhome/groundfloor/livingroom/#
```

Example above means that we will receive all messages with topics which starts with myhome/groundfloor/livingroom (myhome/groundfloor/livingroom/humidity etc.. )

##### Payload
Payload is a message content. The most popular MQTT payload type is json, which was encoded before being sent.

##### MQTT devices

<b>Publisher</b> is a device that sends (publish) message to the MQTT broker. <b>Broker server</b> is responsible for process received messages to the <b>subscribers</b> (end devices) which are subscribing message topic. Many subscribers can subscribe the same topic. Below is diagram describing MQTT protocol in action.
</div>

![](img/mqtt_architecure.png)

---
## System action algorithm
<div align="justify">

<ol>
<li>[Client] Client send message to the server through MQTT broker</li>
<li>[Server] If connection is established, server received message from client</li>
<li>[Server] Server is checking data correctness, for example that message contains appropriate strings like username or request command</li>
<li>[Server] Execute command and prepare response</li>
<li>[Server] Send reponse to the client who send request</li>
<li>[Server] Update local databases - users.json and cars.json</li>
<li>[Client] Client receive reponse from the server through MQTT Broker</li>
</ol>

</div>


---
## Message structure

##### Send by client:
- Topics:
    - /car_rental/client/register_user
    - /car_rental/client/username/login_user
    - /car_rental/client/username/delete_user
    - /car_rental/client/username/logout_user
    - /car_rental/client/username/get_available_cars
    - /car_rental/client/username/get_rented_cars
    - /car_rental/client/username/rent_car
    - /car_rental/client/username/return_car
- Payload:
    - username value
    - password value
    - additional data, for example car id we want to rent

##### Send by server:
- Topics:
    - /car_rental/server/username/register_user
    - /car_rental/server/username/login_user
    - /car_rental/server/username/delete_user
    - /car_rental/server/username/logout_user
    - /car_rental/server/username/get_available_cars
    - /car_rental/server/username/get_rented_cars
    - /car_rental/server/username/rent_car
    - /car_rental/server/username/return_car
- Payload:
    - for register_user, login_user, delete_user, logout_user, rent_car and return_car server returns True of False depends on action result
    - for get_available_cars and server return json file with results

##### Some examples:

##### Login
- Client send request for login:
    - topic: 
    ```python
    /car_rental/client/johnsmith/login_user
    ```
    - payload:
    ```json
    {
        "username": "johnsmith",
        "password": "mypassword1"
    }
    ```
- Server receive message from user *johnsmith* and process login request, check that this user with this password exists. If verification process is correct, server send response:
    - topic: 
    ```python
    /car_rental/server/johnsmith/login_user
    ```
    - payload: 
    ```python
    True
    ```

##### Get available cars
- Client send request for login:
    - topic: 
    ```python
    /car_rental/client/johnsmith/get_available_cars
    ```
    - payload:
    ```json
    {
        "username": "johnsmith",
        "password": "mypassword1"
    }
    ```
- Server receive message from user *johnsmith* and process get_available_cars request, check that this user is logged. If verification process is correct, server send response:
    - topic: 
    ```python
    /car_rental/server/johnsmith/get_available_cars
    ```
    - payload: 
    ```json
    {
        "1": {
            "id": 1,
            "type": "BMW",
            "model": "E46",
            "status": "free"
        },
        "2": {
            "id": 2,
            "type": "AUDI",
            "model": "A3",
            "status": "free"
        },
        "3": {
            "id": 3,
            "type": "FORD",
            "model": "MUSTANG_GT",
            "status": "free"
        }
    }
    ```

---
## Project structure

Project folder structure:

```console
.
├── README.md
├── img
│   ├── agh_logo.png
│   └── mqtt_architecure.png
└── src
    ├── client
    │   ├── ClientManager.py
    │   └── client.py
    ├── configurations
    │   ├── car_data.json
    │   ├── config.json
    │   └── user_data.json
    └── server
        ├── CarRentalManager.py
        ├── ServerManager.py
        └── server.py
```

The **client** folder contains scripts:
- client.py - main client script, loads configuration file, runs mqtt client loop and console user interface in separate threads
- ClientManager.py - contains ClientManager class which is responsible for process mqtt request and display menu. Object of this class is created in client.py file.

The **configurations** folder contains scripts:
- car_data.json - car database, contains information about car type, car model oraz car status (rented or not)
- config.json - contains information about MQTT server credentials
- user_data.json - user database, contains information about user name, surname, rented cars etc.

The **server** folder contains scripts:
- server.py - main server script, loads configuration file, runs server manager object in program loop.
- ServerManager.py - contains ServerManager class which is responsible for process mqtt request and send responses to the requested client.
- CarRentalManager.py - contains CarRentalManager class which is responsible for execute client tasks and update local databases. Object of this class is created in ServerManager.py file.

---
## Running project
#### Preconditions

- MQTT Broker host platform (for example Rasbperry Pi platform or any linux machine)
    - Install mqtt dependencies
        ```console
        sudo apt install mosquitto mosquitto-clients
        ```
    - Enable MQTT broker service
        ```console
        sudo systemctl enable mosquitto
        ```
    - Start MQTT broker service
        ```console
        sudo systemctl start mosquitto.service
        ```
- MQTT Client libraries for python (*Linux and MacOS devices*)
    - Firstly install pip3
        ```console
        curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
        ```
        ```console
        sudo python3 get-pip.py
        ```
    - Download and install python mqtt libraries by typing:
        ```console
        sudo pip3 install paho-mqtt python-etcd
        ```
    
#### Run Project Scripts
- **Configuration**
    - Before you run project scripts
        - Update MQTT broker address in config.json file. If you run broker on localhost machine just  insert localhost ip address. The port is 1883 for unsecure connection.
        - Update configurations_path variable in server.py and client.py. It is a path to *configurations* folder and its different to various operating systems

- **Run server**
    - Enter into folder contains project and open terminal
    - Run server script with debug output True (default is False): 
        ```console
        python3 src/server/server.py --debug=True
        ```
    - If everything started correctly then terminal output should be like this
        ```console
        [MAIN] Server script started
        [INFO] debug: False
        [ServerManager] Starting connection to the server: 192.168.0.248, port: 1883
        [ServerManager] Start server loop
        [ServerManager] Connection successully created
        [ServerManager] Start subscribing topic: /car_rental/server/#
        ```

- **Run client**
    - Enter into folder contains project and open terminal
    - Run server script with debug output True (default is False): 
        ```console
        python3 src/client/client.py --debug=True
        ```
    - If everything started correctly then terminal output should be like this
        ```console
        [MAIN] Client script started
        [INFO] debug: True
        [ClientManager] Starting connection to the server: 192.168.0.248, port: 1883
        [ClientManager] Start server loop
        [ClientManager] Connection successully created
        [ClientManager] Start subscribing topic: /car_rental/client/#

        --------------CLIENT MENU--------------
        | 1. Login                            |
        | 2. Register                         |
        | 3. Exit                             |
        ---------------------------------------
        [ClientManager] Choose option:
        ```
